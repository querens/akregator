Rails.application.routes.draw do
	get 'articles/index'
	get 'articles/scrape'
	get 'articles/latest_news'
	
	root 'articles#index'
end
