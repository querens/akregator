require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get articles_index_url
    assert_response :success
  end
  
  test "check db for articles" do
	  get articles_scrape_url
	  assert_response :redirect
	  assert( Article.count >= 20 )
  end

end
